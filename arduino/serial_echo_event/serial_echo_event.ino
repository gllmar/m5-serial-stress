/*
  Serial Echo Event example

  Derived from https://www.arduino.cc/en/Tutorial/BuiltInExamples/SerialEvent
*/
#include "M5Atom.h"

int baud_selected = 21;

int  baudRates[] = {
110,    // 0
300,    // 1
600,    // 2
1200,   // 3
1800,   // 4
2400,   // 5
4800,   // 6
9600,   // 7
14400,  // 8 
19200,  // 9
38400,  // 10
56000,  // 11
57600,  // 12 
115200, // 13
230400, // 14
250000, // 15
460800, // 16
500000, // 17
750000, // 18
921600, // 19
1000000, // 20
1500000, // 21
2000000, // 22
};

String inputString = "";      // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

void setup() {
  M5.begin(false, false, false);

  // initialize serial:
  Serial.begin(baudRates[baud_selected]);
  delay(100);
  Serial.println("");
  Serial.print("Baudrate is: ");
  Serial.println(baudRates[baud_selected]);
  // reserve 1024 bytes for the inputString:
  inputString.reserve(1024);
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    Serial.println(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
