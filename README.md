# m5-serial-stress

## Speed test

High speed communication (>115200 baud) with different software solution to identify potential bottleneck.

Target : develop a OSC to USB LED driver capable of driving more than 170 RGB pixels @ 44hz

### Pure Data 

* Bottleneck : comport does not support more than 230400 bauds because cfsetospeed is limited 
 
a first approch has been test in this repository : https://gitlab.com/gllmar/duinode-artnet

this code : https://gitlab.com/gllmar/duinode-artnet/-/blob/main/arduino/MicroOsc_NeoPixel_SLIPUSB_M5/MicroOsc_NeoPixel_SLIPUSB_M5.ino

is currently limited with baudrate speeds of comport

#### pd-comport-speed-stress

* create a simple echo arduino program that send back the sent message (to validate communication)

* Fork comport, implement faster speed 

* test on mac, linux and windows

* Pull request upstream comport to implement change in distribution
https://git.iem.at/pd/comport


* Stress test communicaiton

* implement a single LED Driver capable of faster speed

* clone the above 


### Pyserial?

https://pyserial.readthedocs.io/en/latest/pyserial_api.html


Standard values above 115200, such as: 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000 also work on many platforms and devices.

### Openframeworks?

if above fail


![](./brainstorm.drawio.png)
