import argparse
from pythonosc import udp_client
from pythonosc import dispatcher
from pythonosc import osc_server
import serial
import threading
import time

class SerialOSC:
    def __init__(self, osc_in_port, osc_out_port, osc_ip, serial_device, baudrate):
        self.osc_in_port = osc_in_port
        self.osc_out_port = osc_out_port
        self.osc_ip = osc_ip
        self.serial_device = serial_device
        self.baudrate = baudrate
        self.serial_conn = None
        self.osc_client = None
        self.osc_server = None

    def osc_to_serial_handler(self, unused_addr, *args):
        msg = ' '.join(map(str, args))
        if not msg.endswith('\n'):
            msg += '\n'
        self.serial_conn.write(msg.encode())


    def serial_to_osc_handler(self):
        while True:
            if self.serial_conn.inWaiting():
                msg = self.serial_conn.readline().decode().strip('\n')
                self.osc_client.send_message("/serial", msg)
                print(msg, flush=True)
            else:
                time.sleep(0.01)  # Add a small delay when there's no data


    def setup(self):
        self.serial_conn = serial.Serial(self.serial_device, self.baudrate)
        self.osc_client = udp_client.SimpleUDPClient(self.osc_ip, self.osc_out_port)

        disp = dispatcher.Dispatcher()
        disp.map("/serial", self.osc_to_serial_handler)

        self.osc_server = osc_server.ThreadingOSCUDPServer(
            ("localhost", self.osc_in_port), disp)
        print("Serving on {}".format(self.osc_server.server_address))

    def start(self):
        self.setup()
        print("Starting OSC server")
        server_thread = threading.Thread(target=self.osc_server.serve_forever)
        server_thread.start()

        print("Starting Serial to OSC handler")
        serial_thread = threading.Thread(target=self.serial_to_osc_handler)
        serial_thread.start()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--osc_in_port", type=int, default=5005)
    parser.add_argument("--osc_out_port", type=int, default=5006)
    parser.add_argument("--osc_ip", type=str, default="127.0.0.1")
    parser.add_argument("--serial_device", type=str, default="/dev/ttyS0")
    parser.add_argument("--baudrate", type=int, default=9600)
    args = parser.parse_args()

    serial_osc = SerialOSC(args.osc_in_port, args.osc_out_port, args.osc_ip, args.serial_device, args.baudrate)
    serial_osc.start()
