import serial
import time
import argparse

def serial_send_receive(serial_device, baudrate):
    # Open the serial connection
    ser = serial.Serial(serial_device, baudrate)

    # Send a message
    message = "Hello" + "\n"
    print("Sending: " + message)
    ser.write(message.encode())

    # Wait for a response
    time.sleep(0.1)

    # Read the response
    while ser.inWaiting():
        response = ser.readline().decode().strip()
        print("Received: " + response)

    # Close the serial connection
    ser.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--serial_device", type=str, default="/dev/ttyS0")
    parser.add_argument("--baudrate", type=int, default=9600)
    args = parser.parse_args()

    serial_send_receive(args.serial_device, args.baudrate)