from pythonosc import udp_client
from pythonosc import dispatcher
from pythonosc import osc_server
import threading
import time

class OSCClientServerTest:
    def __init__(self, osc_send_port, osc_receive_port, osc_ip):
        self.osc_send_port = osc_send_port
        self.osc_receive_port = osc_receive_port
        self.osc_ip = osc_ip
        self.osc_client = None
        self.osc_server = None

    def handle_response(self, unused_addr, args):
        print("Received response: ", args)

    def setup(self):
        self.osc_client = udp_client.SimpleUDPClient(self.osc_ip, self.osc_send_port)

        disp = dispatcher.Dispatcher()
        disp.map("/response", self.handle_response)

        self.osc_server = osc_server.ThreadingOSCUDPServer(
            (self.osc_ip, self.osc_receive_port), disp)
        print("Serving on {}".format(self.osc_server.server_address))

    def start(self):
        self.setup()
        print("Starting OSC server")
        server_thread = threading.Thread(target=self.osc_server.serve_forever)
        server_thread.start()

        print("Sending OSC message")
        self.osc_client.send_message("/serial", "Hello")

if __name__ == "__main__":
    osc_test = OSCClientServerTest(5005, 5006, "127.0.0.1")
    osc_test.start()
