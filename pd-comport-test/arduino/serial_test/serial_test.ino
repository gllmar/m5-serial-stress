/*
  Serial Event example

  When new serial data arrives, this sketch adds it to a String.
  When a newline is received, the loop prints the string and clears it.

  A good test for this is to try it with a GPS receiver that sends out
  NMEA 0183 sentences.

  NOTE: The serialEvent() feature is not available on the Leonardo, Micro, or
  other ATmega32U4 based boards.

  created 9 May 2011
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/SerialEvent
*/
#include "M5Atom.h"

int  baudRates[] = {
110,    // 0
300,    // 1
600,    // 2
1200,   // 3
1800,   // 4
2400,   // 5
4800,   // 6
9600,   // 7
14400,  // 8 
19200,  // 9
38400,  // 10
56000,  // 11
57600,  // 12 
115200, // 13
230400, // 14
250000, // 15
460800, // 16
500000, // 17
921600, // 18
1000000, // 19
2000000, // 20
};

int baud_selected = 17;

String inputString = "";      // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

void setup() {
  M5.begin(false, false, false);

  // initialize serial:
  Serial.begin(baudRates[baud_selected]);
  delay(100);
  Serial.println("");
  Serial.print("Baudrate is: ");
  Serial.println(baudRates[baud_selected]);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

void loop() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is '\n' (ASCII 10), set a flag
    // so the main loop can do something about it:
    if (inChar == '\n' ) {
      stringComplete = true;
    }
  }

  if (stringComplete) {
    Serial.println(inputString); // print the string when a newline arrives:
    inputString = ""; // clear the string
    stringComplete = false;
  }
}